<?php 
    session_start();
    include("game_functions.php"); 
    $sizeX = 0;
    $sizeY = 0;
    
    // SAVE DONOTRELOAD IF CHECKED
    $_SESSION["doNotAutoReload"] = isset($_GET["doNotAutoReload"]);
    
    if(filter_input(INPUT_GET, "sizeX") != null) { $sizeX = filter_input(INPUT_GET, "sizeX"); }
    if(filter_input(INPUT_GET, "sizeY") != null) { $sizeY = filter_input(INPUT_GET, "sizeY"); }

    //SAVE EN SESSION SI PARAMETRES, SINON RECUP CEUX DE LA SESSION OU SET DEFAULT
    if($sizeX == 0){
        if (isset($_SESSION["sizeX"])){ $sizeX = $_SESSION["sizeX"]; }
        else { $sizeX = 100; }
    }
    else{ $_SESSION["sizeX"] = $sizeX; }
    
    //SAVE EN SESSION SI PARAMETRES, SINON RECUP CEUX DE LA SESSION OU SET DEFAULT
    if($sizeY == 0){
        if (isset($_SESSION["sizeY"])){ $sizeY = $_SESSION["sizeY"]; }
        else { $sizeY = 100; }
    }
    else{ $_SESSION["sizeY"] = $sizeY; }
    
    $cells = [];
    //CREATION DU JEU
    if(isset($_GET["sizeX"])){
        switch($_GET["generation"]){
            default:
            case "random":
                $cells = initTab($sizeX, $sizeY);
                break;
            case "ship":
                $cells = buildVaisseau($sizeX, $sizeY);
                break;
            case "generator":
                if($sizeX < 36){
                    $sizeX = 36;
                    $_SESSION["sizeX"] = $sizeX;
                }
                if($sizeY < 9){
                    $sizeY = 9;
                    $_SESSION["sizeY"] = $sizeY;
                }
                $cells = buildGenerator($sizeX, $sizeY);
                break;
            /*case "cross":
                if($sizeX < 10){
                    $sizeX = 10;
                    $_SESSION["sizeX"] = $sizeX;
                }
                if($sizeY < 10){
                    $sizeY = 10;
                    $_SESSION["sizeY"] = $sizeY;
                }
                $cells = buildCross($sizeX, $sizeY);
                break;*/
        }
    }
    
    // SI CE N'EST PAS LA PREMIERE PARTIE, EXECUTER LA GENERATION
    else{
        $cells = initTab($sizeX, $sizeY);
        console_log("GenerateWorld");
        $cells = updateGeneration($cells, $sizeX, $sizeY);
        saveCells($cells);
    }
    
    //GENERATION DE LA MATRICE
    include("grid_template.php");
