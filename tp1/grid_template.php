<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Life Game</title>
        <link rel="stylesheet" type="text/css" href="main.css">
        <?php 
        if(!isset($_GET["doNotAutoReload"])) {
            echo "<meta http-equiv=\"refresh\" content=\"1; url=./game.php\"/>";
        }
        ?>
    </head>
    <body>
        <div class="affichage"><a href="index.php">Retour au menu</a><?php if($_SESSION["doNotAutoReload"]){ echo " | <a href=\"game.php?doNotAutoReload=on\">Génération suivante</a>";} ?></div>
        <?php
        echo "<table>";
        for($i = 0; $i < $sizeY; $i++){
            echo "<tr>";
            for($j = 0; $j < $sizeX; $j++){
                $val = $cells[$j][$i];
                echo "<td class=\"" . $val . "\"></td>";
            }
            echo "</tr>";
        }
        echo "</table>";
        ?>
    </body>
</html>