<?php 
    session_start();
    $width = isset($_SESSION["sizeX"]) ? $_SESSION["sizeX"] : 100;
    $height = isset($_SESSION["sizeY"]) ? $_SESSION["sizeY"] : 50;
    $checked = isset($_SESSION["doNotAutoReload"]) ? $_SESSION["doNotAutoReload"] : false;
    $delay = isset($_SESSION["delay"]) ? $_SESSION["delay"] : 1;
?>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Life Game</title>
         <link rel="stylesheet" type="text/css" href="main.css">
         <link rel="stylesheet" type="text/css" href="resetForm.css">
    </head>
    <body>
        <div class="affichage">
            <form action="/game.php" method="get">
                <h1>Jeu de la vie</h1>
                <fieldset>
                    <legend>Paramètres</legend>
                    Taille de la grille (X) : <input type="number" name="sizeX" <?php echo "value=\"$width\""; ?> min="5" max="100"><br>
                    Taille de la grille (Y) : <input type="number" name="sizeY" <?php echo "value=\"$height\""; ?> min="5" max="50"><br>
                    Ne pas Auto-Reload : <input type="checkbox" name="doNotAutoReload" <?php if($checked){ echo "checked";} ?>><br>
                </fieldset><br>
                <fieldset>
                    <legend>Type de génération</legend>
                    <input type="radio" name="generation" value="random" checked> Aléatoire<br>
                    <input type="radio" name="generation" value="ship"> Vaisseau<br>
                    <input type="radio" name="generation" value="generator"> Générateur<br>
                </fieldset>
                <div class="affichage special"><input type="submit" value="Voir les générations"></div>
        </div>
    </body>
</html>
