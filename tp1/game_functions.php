<?php

function initTab($sizeX, $sizeY){
    $cells = getSavedCellsOrCreate($sizeX, $sizeY);
    return $cells;
}

function getSavedCellsOrCreate($sizeX, $sizeY){
    //SI ON VIENS DE LANCER LE JEU, RECREER LA VERSION
    $needRecreate = isset($_GET["sizeX"]) ? true : false;
    
    if(!isset($_SESSION["cells"]) || $needRecreate){
        $cells = generateCells($sizeX, $sizeY);
        saveCells($cells);
        return $cells;
    } else {
        return $_SESSION["cells"];
    }
}

function generateCells($sizeX, $sizeY){
    //GENERATION ALEATOIRE DE LA MATRICE DE CELLULES
    $cells = [];
    for($y = 0; $y < $sizeY; $y++){
        for($x = 0; $x < $sizeX; $x++){
            $cells[$x][$y] = rand() % 10 == 0 ? 'o' : ' ';
        }
    }
    
    return $cells;
}

function buildVaisseau($sizeX, $sizeY){
    $cells = [];
    for($y = 0; $y < $sizeY; $y++){
        for($x = 0; $x < $sizeX; $x++){
            $cells[$x][$y] = ' ';
        }
    }
    //Construire le vaisseau
    $cells[0][$sizeY-3] = 'o';
    $cells[1][$sizeY-3] = 'o';
    $cells[2][$sizeY-3] = 'o';
    $cells[2][$sizeY-2] = 'o';
    $cells[1][$sizeY-1] = 'o';
    saveCells($cells);
    return $cells;
}

function buildGenerator($sizeX, $sizeY){
    $cells = [];
    for($y = 0; $y < $sizeY; $y++){
        for($x = 0; $x < $sizeX; $x++){
            $cells[$x][$y] = ' ';
        }
    }
    //Construire le vaisseau
    $cells[0][4] = 'o';
    $cells[0][5] = 'o';
    $cells[1][4] = 'o';
    $cells[1][5] = 'o';
    
    $cells[34][2] = 'o';
    $cells[34][3] = 'o';
    $cells[35][2] = 'o';
    $cells[35][3] = 'o';
    
    $cells[24][0] = 'o';
    $cells[24][1] = 'o';
    $cells[22][1] = 'o';
    $cells[20][2] = 'o';
    $cells[21][2] = 'o';
    $cells[20][3] = 'o';
    $cells[21][3] = 'o';
    $cells[20][4] = 'o';
    $cells[21][4] = 'o';
    $cells[22][5] = 'o';
    $cells[24][5] = 'o';
    $cells[24][6] = 'o';
    
    $cells[17][5] = 'o';
    $cells[16][4] = 'o';
    $cells[16][5] = 'o';
    $cells[16][6] = 'o';
    $cells[15][3] = 'o';
    $cells[15][7] = 'o';
    $cells[14][5] = 'o';
    $cells[13][2] = 'o';
    $cells[12][2] = 'o';
    $cells[11][3] = 'o';
    $cells[10][4] = 'o';
    $cells[10][5] = 'o';
    $cells[10][6] = 'o';
    $cells[11][7] = 'o';
    $cells[12][8] = 'o';
    $cells[13][8] = 'o';
    saveCells($cells);
    return $cells;
}

/*function buildCross($sizeX, $sizeY){
    $cells = [];
    for($y = 0; $y < $sizeY; $y++){
        for($x = 0; $x < $sizeX; $x++){
            $cells[$x][$y] = ' ';
        }
    }
    //Construire le vaisseau
    if($sizeX % 2 == 1)
        $sizeX++;
    if($sizeY % 2 == 1)
        $sizeY++;
    $cells[$sizeX/2][$sizeY/2] = 'o';
    $cells[$sizeX/2+1][$sizeY/2] = 'o';
    $cells[$sizeX/2+2][$sizeY/2] = 'o';
    $cells[$sizeX/2+3][$sizeY/2] = 'o';
    $cells[$sizeX/2][$sizeY/2+1] = 'o';
    $cells[$sizeX/2][$sizeY/2+2] = 'o';
    $cells[$sizeX/2][$sizeY/2+3] = 'o';
    $cells[$sizeX/2-1][$sizeY/2] = 'o';
    $cells[$sizeX/2-2][$sizeY/2] = 'o';
    $cells[$sizeX/2-3][$sizeY/2] = 'o';
    $cells[$sizeX/2][$sizeY/2-1] = 'o';
    $cells[$sizeX/2][$sizeY/2-2] = 'o';
    $cells[$sizeX/2][$sizeY/2-3] = 'o';
    $cells[$sizeX/2+3][$sizeY/2-1] = 'o';
    $cells[$sizeX/2+3][$sizeY/2-2] = 'o';
    $cells[$sizeX/2-3][$sizeY/2+1] = 'o';
    $cells[$sizeX/2-3][$sizeY/2+2] = 'o';
    $cells[$sizeX/2-1][$sizeY/2-3] = 'o';
    $cells[$sizeX/2-2][$sizeY/2-3] = 'o';
    $cells[$sizeX/2+1][$sizeY/2+3] = 'o';
    $cells[$sizeX/2+2][$sizeY/2+3] = 'o';
    saveCells($cells);
    return $cells;
}*/

function saveCells($cells){
    $_SESSION["cells"] = $cells;
}

function updateGeneration($cells, $sizeX, $sizeY){
    $nextCells = $cells;
    for($ycell = 0; $ycell < $sizeY; $ycell++){
        for($xcell = 0; $xcell < $sizeX; $xcell++){
            $nextCells[$xcell][$ycell] = checkCell($cells, $xcell, $ycell, $sizeX, $sizeY);
        }
    }
    return $nextCells;
}

function checkCell($cells, $x, $y, $sizeX, $sizeY){
    $counter = 0;
    $cellState = $cells[$x][$y];
    
    //console_log("$cellState at pos $x $y");
    
    for($posY = $y-1; $posY <= $y+1; $posY++){
        for($posX = $x-1; $posX <= $x+1; $posX++){
            if($posY >= 0 && $posY < $sizeY){
                if($posX >= 0 && $posX < $sizeX){
                    if(!($posX == $x && $posY == $y)){
                        if($cells[$posX][$posY] != ' ')
                            $counter++;
                        //console_log("--- $posX $posY");
                    }
                }
            }
        }
    }
    if($counter == 3){
        $cellState = 'o';
        //console_log("Cell created");
    }
    if($cellState == 'o') {
        if($counter > 3 || $counter < 2)
            $cellState = ' ';
    }
    return $cellState;
}

//LOG DANS LA CONSOLE
function console_log( $data ){
  echo '<script>';
  echo 'console.log('. json_encode( $data ) .')';
  echo '</script>';
}