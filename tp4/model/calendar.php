<?php 
session_start();
include("events.php");
function createCalendar(){
	echo "<table class=\"calendar\">";
	echo "<tr><td>Lundi</td><td>Mardi</td><td>Mercredi</td><td>Jeudi</td><td>Vendredi</td><td>Samedi</td><td>Dimanche</td></tr>";
	$nbrDays = cal_days_in_month(CAL_GREGORIAN, getMonth(), getYear());

	$dowMap = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
	$firstDay = date("N", strtotime("first day of " . $dowMap[getMonth()-1] . " " . getYear()));
	$j = 1;

	//echo $dowMap[getMonth()-1] . ", " . getYear();

	// Afficher le Jour 1 au jour correspondant (lundi, mardi, ...)
	while($j < $firstDay){
		
		if($j % 7 == 1) echo "<tr>";
		echo "<td>-</td>";
		if($j % 7 == 0) echo "</tr>";
		
		$j++;
	}

	for($i = 1; $i <= $nbrDays; $i++){

		if(($i+($firstDay-1)) % 7 == 1) echo "<tr>";
		echo "<td class=\"case\"><div class=\"day\">" . $i . "</div><div class=\"informations\">";
		
		// CONTENU
		$date = new DateTime(getYear() . "-" . getMonth() . "-" . $i);
		$events = getEventsPerDay($date->format('Y-m-d H:i'));
		if(!empty($events)){
			$eventCounter = 0;
			foreach($events as $event) {
				$eventCounter++;
				if($eventCounter <= 5){
				//echo "<div class=\"event\">";
					// ($event['nb_place'] - getRemainingSeats($event['id'])) . "/" . $event['nb_place']
					echo $event['name'] . " - " . getRemainingSeats($event['id']) . " participant(s).";
					echo " <a class=\"eventButton\" href=\"../view/event.php?eventID=" . $event['id'] . "\">Voir</a><br>";
				}
			}
			if($eventCounter > 5){
				$_SERVER['selectedDay'] = $i;
				echo "<a class=\"eventButton\" href=\"../view/eventsDate.php?selectedDay=" . $i . "\">Voir tout les événements</a>";
			}
		}
		else echo "Aucun événement pour ce jour disponible.";
		echo "</div></td>";
		if(($i+($firstDay-1)) % 7 == 0) echo "</tr>";
	}
	echo "</table>";
}

function getMonth(){
	if(isset($_SESSION['month']))
		$month = htmlspecialchars($_SESSION['month']);
	else
		$month = $_SESSION['month'] = date("m");
	return $month;
}

function getYear(){
	if(isset($_SESSION['year']))
		$year = $_SESSION['year'];
	else
		$year = $_SESSION['year'] = date('Y');
	return $year;
}

function addMonth(){
	if($_SESSION['month'] == 12){
		$_SESSION['month'] = 1;
		$_SESSION['year'] = $_SESSION['year'] + 1;
	}
	else
		$_SESSION['month'] = $_SESSION['month'] + 1;
}

function removeMonth(){
	if($_SESSION['month'] == 1){
		$_SESSION['month'] = 12;
		$_SESSION['year'] = $_SESSION['year'] - 1;
	}
	else
		$_SESSION['month'] = $_SESSION['month'] - 1;
}