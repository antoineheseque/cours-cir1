<?php
function openBDD(){
	try{
		$bdd = new PDO('mysql:host=localhost;dbname=calendar;charset=utf8', 'calendar', 'calendar');
	} catch(Exception $e){
		exit ("Erreur" . $e -> getMessage());
	}
	return $bdd;
}