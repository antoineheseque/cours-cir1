<?php
include("bdd.php");

# EVENTS
function addEvent($name, $desc, $startDate, $endDate, $availableSeats){
	$bdd = openBDD();
	$addEvent = $bdd->prepare("INSERT INTO events(name, description, startdate, enddate, organizer_id, nb_place) VALUES(?, ?, ?, ?, ?, ?)");
	$addEvent->execute(array($name, $desc, $startDate, $endDate, $_SESSION['userID'], $availableSeats));
}

function removeEvent($id){
	$bdd = openBDD();

	$removeLinks = $bdd->prepare("DELETE FROM user_participates_events WHERE id_event = ?");
	$removeLinks->execute(array($id));

	$removeEvent = $bdd->prepare("DELETE FROM events WHERE id = ?");
	$removeEvent->execute(array($id));
}

function getEvent($id){
	$bdd = openBDD();
  	$request = $bdd->prepare("SELECT * FROM events WHERE id = ?");
    $request->execute(array($id));
    $event = $request->fetchAll();
	return $event;
}

function getEventsPerDay($day){
	$bdd = openBDD();
  	if($_SESSION['connected'] == 'organizer'){
    	$request = $bdd->prepare("SELECT * FROM events WHERE startdate <= ? AND enddate >= ? AND organizer_id = ?");
    	$request->execute(array($day, $day, $_SESSION['userID']));
  	}
	else {
  		$request = $bdd->prepare("SELECT * FROM events WHERE startdate <= ? AND enddate >= ?");
  		$request->execute(array($day, $day));
	}
	$events = $request->fetchAll();
	return $events;
}

function isAlreadyJoigned($id){
	$bdd = openBDD();
	$userID = ($_SESSION['connected'] == 'organizer') ? 1 : 2;
	$request = $bdd->prepare("SELECT * FROM user_participates_events WHERE id_event = ? AND id_participant = ?");
	$request->execute(array($id, $userID));
	$result = $request->fetchAll();
	
	return empty($result) ? false : true;
}

function getRemainingSeats($id){
	$bdd = openBDD();
	$request = $bdd->prepare("SELECT COUNT(*) AS nbr FROM user_participates_events WHERE id_event=?");
	$request->execute(array($id));
	$nbr = $request->fetchAll();
	return $nbr[0][0];
}

function joinEvent($userID, $eventID){
	$bdd = openBDD();
	$addEvent = $bdd->prepare("INSERT INTO user_participates_events(id_participant, id_event) VALUES(?, ?)");
	$addEvent->execute(array($userID, $eventID));
}