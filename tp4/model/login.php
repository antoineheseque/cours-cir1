<?php
session_start();

include("bdd.php");

// LOGIN
function login($username, $password){
	$bdd = openBDD();

	// Get USER Line with login $username
	$result = $bdd->prepare("SELECT * FROM Users WHERE login = ?");
	$result->execute(array($username));
	$user = $result->fetch();
	
	// Check if password match
	if(password_verify($password, $user['password'])){
		$_SESSION['connected'] = $user['login'];
		$_SESSION['userID'] = $user['id'];
		return true;
	}
	return false;
}