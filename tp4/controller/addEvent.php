<?php
session_start();
include_once("../model/events.php");
if(isset($_POST['eventName'])){
	$name = filter_input(INPUT_POST, 'eventName');
	$desc = filter_input(INPUT_POST, 'eventDescription');
	$startDate = filter_input(INPUT_POST, 'eventStartDate');
	$endDate = filter_input(INPUT_POST, 'eventEndDate');
	$availableSeats = filter_input(INPUT_POST, 'eventSeatsNumber');
	addEvent($name, $desc, $startDate, $endDate, $availableSeats);
	//addEvent("Annivdz", "Descrifzfption", DateTime(2018,08,28), DateTime(2018,08,28), 100);
}
header("Location: ../");