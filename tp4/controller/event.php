<?php 
	include_once("../model/calendar.php");
	$id = filter_input(INPUT_GET, 'eventID');
	if(empty($id)){
		header("Location: ../view/calendar.php");
	}

	$event = getEvent($id);
	$alreadyJoigned = isAlreadyJoigned($id);
	$startDate = new DateTime($event[0]['startdate']);
	$endDate = new DateTime($event[0]['enddate']);
	echo "<legend>EVENEMENT: " . $event[0]['name'] . "</legend>";
	echo $event[0]['description'];
	echo "<br><br>Début de l'événement: " . $startDate->format('d-m-Y');
	echo "<br>Fin de l'événement: " . $endDate->format('d-m-Y');
	echo "<br><br><b>Vous " . ($alreadyJoigned ? "êtes" : "n'êtes pas") . " déjà inscrit.</b>";
	$count = ($event[0]['nb_place'] - getRemainingSeats($id));
	echo "<br> Il reste actuellement " . $count . " place(s).";

	$rank = filter_var($_SESSION['connected']);
	if($rank == "customer"){
		if(!$alreadyJoigned && $count > 0)
			echo "<br><br><a href=\"../controller/joinEvent.php?eventID=" . $id . "\">Participer à l'événement</a>"; 
	}

	else if($rank == "organizer"){
		if(!$alreadyJoigned && $count > 0)
			echo "<br><br><a href=\"../controller/joinEvent.php?eventID=" . $id . "\">Participer à l'événement</a>"; 

		echo "<br><a href=\"../controller/removeEvent.php?eventID=" . $id . "\">Supprimer l'événement</a>"; 
	}

	echo "<br><br><a href=\"../view/calendar.php\">Retour au calendrier</a>";