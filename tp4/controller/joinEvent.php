<?php 
	include_once("../model/calendar.php");
	$eventID = filter_input(INPUT_GET, 'eventID');
	$userID = filter_var($_SESSION['connected']) == "organizer" ? 1 : 2;

	if(empty($id)){
		header("Location: ../view/calendar.php");
	}

	joinEvent($userID, $eventID);
	header("Location: ../view/event.php?eventID=" . $eventID);