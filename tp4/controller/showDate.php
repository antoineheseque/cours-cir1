<?php
	include_once("../model/calendar.php");
	$months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
	$date = getMonth();
	echo $months[$date-1] . " " . getYear();