<?php 
	include_once("../model/calendar.php");
	$day = filter_input(INPUT_GET, 'selectedDay');

	$rank = filter_var($_SESSION['connected']);

	if(empty($day)){
		header("Location: ../view/calendar.php");
	}
	echo "<legend>EVENEMENTS DU " . $day . "/" . getMonth() . "/" . getYear() . "</legend>";
	$date = new DateTime(getYear() . "-" . getMonth() . "-" . $day);
	$events = getEventsPerDay($date->format('Y-m-d H:i'));
	if(!empty($events)){
		foreach($events as $event) {
			if(!($rank == "customer" && ($event['nb_place'] - getRemainingSeats($event['id'])) == 0)){
				
				$alreadyJoigned = isAlreadyJoigned($event['id']);
				if($alreadyJoigned)
					echo "<b>(inscrit) </b>";

				echo $event['name'] . " - " . ($event['nb_place'] - getRemainingSeats($event['id'])) . " place(s) restante(s).";
				echo " <a class=\"eventButton\" href=\"../view/event.php?eventID=" . $event['id'] . "\">Voir</a>";
				echo "<br>";
			}
		}
	}
	echo "<br><a href=\"../view/calendar.php\">Retour au calendrier</a>";