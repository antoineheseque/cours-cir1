<?php
include_once("../model/calendar.php");
$rank = filter_var($_SESSION['connected']);

if($rank == "organizer" || $rank == "customer"){
	//show calendar here
	createCalendar();
}
else{
	header("Location: ../view/login.php");
}
