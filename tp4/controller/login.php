<?php
// Include database functions
include_once("../model/login.php");

if(!empty($_POST['user']) && !empty($_POST['password'])){

	if(login(filter_input(INPUT_POST, 'user'), filter_input(INPUT_POST, 'password'))){
		// Move to CALENDAR
		header("Location: ../view/calendar.php");
		exit();

	}
	$_SESSION['error'] = "Mauvais nom d'utilisateur / mot de passe!";
	header("Location: ../");
	exit();
}

$_SESSION['error'] = "Il faut renseigner nom d'utilisateur ET mot de passe!";
header("Location: ../");