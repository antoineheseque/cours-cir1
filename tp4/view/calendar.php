<!DOCTYPE html>
<html>
<head>
	<title>Calendar</title>
	<link rel="stylesheet" type="text/css" href="../css/calendar.css">
</head>
<body>
	<a href="../controller/logout.php" class="disconnect">Se déconnecter</a>
	<div class="header">
		<a href="../controller/removeMonth.php">Précédent</a>
		 - <?php include_once("../controller/showDate.php"); ?> - 
		<a href="../controller/addMonth.php">Suivant</a>
	</div>

	<?php include("../controller/calendar.php"); ?>
	
	<!-- ADD ORGANIZER BAR -->
	<?php if ($_SESSION['connected'] == "organizer"): ?>
	<div class="specialBar">
		<form class="createEvent" method="POST" action="../controller/addEvent.php">
			<input type="text" name="eventName" placeholder="Nom de l'événement" required> <input style="width:280px;" type="text" name="eventDescription" placeholder="Description"> <input style="width:160px;" type="number" min="1" max="1000" name="eventSeatsNumber" placeholder="Nombre de places" required> Date de début: <input type="date" name="eventStartDate" required> Date de fin: <input type="date" name="eventEndDate" required> <input type="submit" name="Créer">
		</form>
	</div>
	<?php endif; ?>

	<!-- ADD CUSTOMER BAR -->
	<?php if ($_SESSION['connected'] == "customer"): ?>
	<div class="specialBar">
		Cliquez sur un jour pour voir les événements associés.
	</div>
	<?php endif; ?>
</body>
</html>