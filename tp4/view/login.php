<!DOCTYPE html>
<html>
<head>
	<title>Calendar - Connexion</title>
	<link rel="stylesheet" type="text/css" href="../css/calendar.css">
</head>
<body>
<form method="POST" action="../controller/login.php">
	<fieldset>
		<legend>Panel de Connexion</legend>
		Nom d'utilisateur: <input type="text" name="user">
		<br>
		Mot de passe: <input type="password" name="password">
		<br>
		<input type="submit" name="Valider">
	</fieldset>
	<?php 
	session_start();
	if(!empty($_SESSION['error'])){
		echo "<h4 style=\"color: red\">" . htmlspecialchars($_SESSION['error']) . "</h4>"; 
		$_SESSION['error'] = "";
	}
	?>
</form>
</body>
</html>