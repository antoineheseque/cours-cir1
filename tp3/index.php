<?php include("messages.php");?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>particles.js</title>
  <meta name="description" content="particles.js is a lightweight JavaScript library for creating particles.">
  <meta name="author" content="Vincent Garreau" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" media="screen" href="css/style.css">
</head>
<body>

<div class="container">
  <?php showMessages() ?>
</div>
<div class="form">
  <form method="post" action="./sendMessage.php">
    <input type="text" name="username" placeholder="Nom d'utilisateur"><input type="textarea" name="message" placeholder="Entrez votre message ..."><input type="submit" name="Valider">
  </form>
</div>

<!-- particles.js container -->
<div id="particles-js"></div>

<!-- scripts -->
<script src="js/particles.js"></script>
<script src="js/app.js"></script>
</body>
</html>