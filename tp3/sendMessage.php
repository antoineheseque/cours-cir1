<?php 
if(isset($_POST['username']) && isset($_POST['message'])){
	createMessage(filter_input(INPUT_POST, message), filter_input(INPUT_POST, username));
}

function createMessage($message, $username){
	$date = date('Y-m-d H:i:s');
	try {
	   $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
	   $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'chat', 'aM%l6v29', $opts);
	   $query = $bdd->prepare("INSERT INTO messages(username, message, dateM) VALUES (?, ?, ?)");
	   $query -> execute(array($username, $message, $date));
	} catch(PDOException $e){ echo $e; }
}

header("Location: index.php#text");