<?php

function showMessages(){
	try {
	   $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
	   $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'chat', 'aM%l6v29', $opts);
	   $resultCursor = $bdd->query('SELECT * FROM messages ORDER BY dateM ASC');
	   foreach($resultCursor as $row) {
	   		if(strpos($row['message'], '/me ') === 0)
	   		{
	   			$message = str_replace('/me', '', $row['message']);
				echo "<p class=\"message me\">[" . $row['dateM'] . "] " . htmlspecialchars($row['username']) .htmlspecialchars($message) . "</p>";
	   		} else {
	   			echo "<p class=\"message\">[" . $row['dateM'] . "] " . htmlspecialchars($row['username']) . ": " . htmlspecialchars($row['message']) . "</p>";
	   		}
	        
	    }
	    echo "<p id=\"text\">Pour envoyer un message, écrivez votre message et cliquez sur envoyer!";
	} catch (Exception $e) {
	        echo "Erreur de connexion à la base de données.";
	}
}