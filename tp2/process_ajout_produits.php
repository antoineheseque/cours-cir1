<?php

/* 
 * The MIT License
 *
 * Copyright 2018 antoine.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$error = [];
if(!empty($_POST['name'])){
    //$error['name'] = 'Fonction name incomplete.';
    $name = $_POST['name'];
    if(preg_match("/[^A-Za-z0-9 ]/", $name)){
         $error['name'] = "Le nom choisi à un format incorrect.";
    }
}
else { $error['name'] = 'Veuillez choisir un nom pour ce produit.'; }

if(!empty($_FILES['img'])){
    define('TARGET_DIRECTORY', './images/products/');
    
    if ( ! is_dir(TARGET_DIRECTORY)) {
        mkdir(TARGET_DIRECTORY);
    }
    
    $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
    $extension_upload = strtolower(  substr(  strrchr($_FILES['img']['name'], '.')  ,1)  );
    if (in_array($extension_upload,$extensions_valides)){
        $nameImg = $_FILES['img']['name'];
        $random = md5($name . time());
        $imgAdress = TARGET_DIRECTORY . $random . $nameImg; // Adress du fichier
        move_uploaded_file($_FILES['img']['tmp_name'], $imgAdress);
    }
    else{
        $error['img'] = 'Format de l\'image incorrect. Formats acceptés: JPG, JPEG, GIF, PNG.';
    }
    
}
else { $error['img'] = 'Veuillez envoyer une image.'; }

if(!empty($_POST['price'])){
    $price = $_POST['price'];
    $splitted = explode('.',$price);
    if(count($splitted) == 1)
        $splitted[1] = 0; 
    if($price < 0 || strlen($splitted[1]) > 2){
        $error['price'] = 'Le prix entré est incorrect.';
    }
}
else { $error['price'] = 'Veuillez entrer un prix.'; }

if(!empty($_POST['number'])){
    $number = $_POST['number'];
    if($number <= 0){
        $error['number'] = 'Nombre inférieur à 0.';
    }
}
else { $error['number'] = 'Veuillez entrer le nombre de produits disponibles.'; }

if(empty($error)){
    // Executer ajout produit ici
    $product = array($name, $imgAdress, $price, $number);
    $fp = fopen('products.csv', 'a');
    fputcsv($fp, $product);
    fclose($fp);
    $error['success'] = 'Produit ajouté avec succès !';
}

include("ajout_produits.php");
unset($error);