<?php

/* 
 * The MIT License
 *
 * Copyright 2018 antoine.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
if(!file_exists('products.csv')) { die("Aucun produits en stock"); }
$fp = fopen('products.csv', 'r');
$product = [];
$counter = 0;
while(($buffer = fgets($fp, 4096)) != false){
    $product[$counter] = $buffer; 
    $counter++;
}
fclose($fp);

if(!empty($product)){
    for($i = 0; $i < count($product); $i++){
        if($i % 3 == 0) { echo "<div class=\"row\">"; }
        
        $parameters = explode(',', $product[$i]);
        
        echo "<div class=\"col-sm-4\"><div class=\"card text-center\"><div class=\"card-body\">";
        echo "<img class=\"card-img-top\" src=\"" . htmlspecialchars($parameters[1]) . "\" alt=\"Image du produit\">";
        echo "<h4 class=\"card-title\">" . htmlspecialchars($parameters[0]) . " - " . htmlspecialchars($parameters[2]) . " €</h4>";
        echo "<a class=\"btn btn-outline-dark\">Acheter - " . htmlspecialchars($parameters[3]) . " produit(s) restant(s).</a>";
        echo "</div></div></div>";
        
        if($i % 3 == 2) { echo "</div><br>"; }
    }
}
else{
    die("Erreur lors du chargement de la liste des objets.");
}

/*
 * <div class="row">
 * <div class="col">
 */