<? session_start(); ?>
<html>
<head>
  <title>Beer' ISEN</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img src="/images/beer.png" width="30" height="30" class="d-inline-block align-top" alt="Beer Logo"><a class="navbar-brand" href="index.php">Beer' ISEN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-item nav-link" href="index.php">Accueil</a>
            <a class="nav-item nav-link" href="listeProduits.php">Liste des produits</a>
            <a class="nav-item nav-link active" href="ajout_produits.php">Ajouter un produit <span class="sr-only">(current)</span></a>
          </div>
        </div>
    </nav>
    <br>
    <div class="container">
        <?php
            
            if(!empty($error) && is_array($error)){
                
                foreach ($error as $key => $value) {
                    switch ($key) {
                        case "name":
                        case "img":
                        case "price":
                        case "number":
                            echo "<div class=\"alert alert-danger\" role=\"alert\">" . htmlspecialchars($value) . "</div>\n";
                            break;
                        case "success":
                            echo "<div class=\"alert alert-success\" role=\"success\">" . htmlspecialchars($value) . "</div>\n";
                            break;
                        default:
                            break;
                    }
                }
            }
        ?>
        <form action="./process_ajout_produits.php" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col">
                <div class="form-group">
                  <label for="nameProduct">Nom du produit</label>
                  <input name="name" type="text" class="form-control" id="inputName" aria-describedby="nameHelp" placeholder="Entrez le nom du produit">
                  <small id="nameHelp" class="form-text text-muted">Le nom du produit doit contenir seulement chiffre et lettres.</small>
                </div>

                <div class="form-group">
                    <label for="imgProduct">Image du produit</label>
                    <input type="file" class="form-control" id="imgProduct" name="img">
                </div>

                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="inputName">Prix du produit</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">€</div>
                        </div>
                        <input name="price" type="number" min="1" max="1000000" step="0.01" class="form-control" id="priceProduct" aria-describedby="priceHelp" placeholder="Entrez le prix du produit">
                    </div>
                    <small id="priceHelp" class="form-text text-muted">Nombre positif avec 2 chiffres après la virgule maximum.</small>
                  </div>
                  <div class="form-group">
                    <label for="inputName">Nombre de produits disponibles</label>
                    <input name="number" type="number" min="0" max="1000000" step="1" class="form-control" id="productNumber" placeholder="Entrez le nombre de produits disponibles">
                  </div>
                </div>
            </div>
            <button type="submit" class="btn btn-outline-dark btn-lg btn-block">Ajouter le produit</button>
        </form>
    </div>
</body>
</html>
