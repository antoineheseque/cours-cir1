<? session_start(); ?>
<html>
<head>
  <title>Beer' ISEN</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <img src="/images/beer.png" width="30" height="30" class="d-inline-block align-top" alt="Beer Logo"><a class="navbar-brand" href="index.php">Beer' ISEN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-item nav-link" href="index.php">Accueil</a>
            <a class="nav-item nav-link active" href="listeProduits.php">Liste des produits <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="ajout_produits.php">Ajouter un produit</a>
          </div>
        </div>
    </nav>
    <br>
    <div class="container">
        <?php include("process_listProduits.php"); ?>
    </div>
</body>
</html>